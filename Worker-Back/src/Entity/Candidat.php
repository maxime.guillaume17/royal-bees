<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CandidatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CandidatRepository::class)]
#[ApiResource]
class Candidat extends User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $hobbies;

    #[ORM\Column(type: 'string', length: 255)]
    private $competences;

    #[ORM\Column(type: 'string', length: 255)]
    private $experiances;

    #[ORM\Column(type: 'string', length: 255)]
    private $ville;

    #[ORM\ManyToMany(targetEntity: metier::class)]
    private $id_metier;

    #[ORM\ManyToMany(targetEntity: offre::class, inversedBy: 'candidats')]
    private $id_offre;

    public function __construct()
    {
        $this->id_metier = new ArrayCollection();
        $this->id_offre = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHobbies(): ?string
    {
        return $this->hobbies;
    }

    public function setHobbies(string $hobbies): self
    {
        $this->hobbies = $hobbies;

        return $this;
    }

    public function getCompetences(): ?string
    {
        return $this->competences;
    }

    public function setCompetences(string $competences): self
    {
        $this->competences = $competences;

        return $this;
    }

    public function getExperiances(): ?string
    {
        return $this->experiances;
    }

    public function setExperiances(string $experiances): self
    {
        $this->experiances = $experiances;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection<int, metier>
     */
    public function getIdMetier(): Collection
    {
        return $this->id_metier;
    }

    public function addIdMetier(metier $idMetier): self
    {
        if (!$this->id_metier->contains($idMetier)) {
            $this->id_metier[] = $idMetier;
        }

        return $this;
    }

    public function removeIdMetier(metier $idMetier): self
    {
        $this->id_metier->removeElement($idMetier);

        return $this;
    }

    /**
     * @return Collection<int, offre>
     */
    public function getIdOffre(): Collection
    {
        return $this->id_offre;
    }

    public function addIdOffre(offre $idOffre): self
    {
        if (!$this->id_offre->contains($idOffre)) {
            $this->id_offre[] = $idOffre;
        }

        return $this;
    }

    public function removeIdOffre(offre $idOffre): self
    {
        $this->id_offre->removeElement($idOffre);

        return $this;
    }
}
