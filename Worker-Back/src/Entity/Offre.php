<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OffreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OffreRepository::class)]
#[ApiResource]
class Offre
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $description;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $salaire;

    #[ORM\Column(type: 'string', length: 255)]
    private $ville;

    #[ORM\Column(type: 'string', length: 255)]
    private $competences;

    #[ORM\OneToMany(mappedBy: 'offre', targetEntity: metier::class)]
    private $id_metier;

    #[ORM\ManyToMany(targetEntity: Candidat::class, mappedBy: 'id_offre')]
    private $candidats;

    #[ORM\ManyToOne(targetEntity: Recruteur::class, inversedBy: 'id_offre')]
    #[ORM\JoinColumn(nullable: false)]
    private $recruteur;

    public function __construct()
    {
        $this->id_metier = new ArrayCollection();
        $this->candidats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSalaire(): ?int
    {
        return $this->salaire;
    }

    public function setSalaire(?int $salaire): self
    {
        $this->salaire = $salaire;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCompetences(): ?string
    {
        return $this->competences;
    }

    public function setCompetences(string $competences): self
    {
        $this->competences = $competences;

        return $this;
    }

    /**
     * @return Collection<int, metier>
     */
    public function getIdMetier(): Collection
    {
        return $this->id_metier;
    }

    public function addIdMetier(metier $idMetier): self
    {
        if (!$this->id_metier->contains($idMetier)) {
            $this->id_metier[] = $idMetier;
            $idMetier->setOffre($this);
        }

        return $this;
    }

    public function removeIdMetier(metier $idMetier): self
    {
        if ($this->id_metier->removeElement($idMetier)) {
            // set the owning side to null (unless already changed)
            if ($idMetier->getOffre() === $this) {
                $idMetier->setOffre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Candidat>
     */
    public function getCandidats(): Collection
    {
        return $this->candidats;
    }

    public function addCandidat(Candidat $candidat): self
    {
        if (!$this->candidats->contains($candidat)) {
            $this->candidats[] = $candidat;
            $candidat->addIdOffre($this);
        }

        return $this;
    }

    public function removeCandidat(Candidat $candidat): self
    {
        if ($this->candidats->removeElement($candidat)) {
            $candidat->removeIdOffre($this);
        }

        return $this;
    }

    public function getRecruteur(): ?Recruteur
    {
        return $this->recruteur;
    }

    public function setRecruteur(?Recruteur $recruteur): self
    {
        $this->recruteur = $recruteur;

        return $this;
    }
}
