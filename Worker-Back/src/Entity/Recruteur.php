<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RecruteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecruteurRepository::class)]
#[ApiResource]
class Recruteur extends User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $logo;

    #[ORM\Column(type: 'string', length: 255)]
    private $entreprise;

    #[ORM\OneToMany(mappedBy: 'recruteur', targetEntity: offre::class, orphanRemoval: true)]
    private $id_offre;

    public function __construct()
    {
        $this->id_offre = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getEntreprise(): ?string
    {
        return $this->entreprise;
    }

    public function setEntreprise(string $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return Collection<int, offre>
     */
    public function getIdOffre(): Collection
    {
        return $this->id_offre;
    }

    public function addIdOffre(offre $idOffre): self
    {
        if (!$this->id_offre->contains($idOffre)) {
            $this->id_offre[] = $idOffre;
            $idOffre->setRecruteur($this);
        }

        return $this;
    }

    public function removeIdOffre(offre $idOffre): self
    {
        if ($this->id_offre->removeElement($idOffre)) {
            // set the owning side to null (unless already changed)
            if ($idOffre->getRecruteur() === $this) {
                $idOffre->setRecruteur(null);
            }
        }

        return $this;
    }
}
